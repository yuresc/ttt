//make boardState an array
//remove Object.values()

let readlineSync = require('readline-sync');

let targetRow = 0;
let targetCol = 0;
let maxRowLength = 3;

const findSpace = (row, col) => {
    result = row * maxRowLength + col;
    return result;
};

console.assert(findSpace(0, 0) === 0, '0,0 is not 0')
console.assert(findSpace(1, 1) === 4, '1,1 is not 4')
console.assert(findSpace(1, 2) === 5, '1,2 is not 5')

let boardState = [];

for (let space = 0; space < 9; space++) {
    boardState.push('_');
}

let gameState = {
    markList: ["X", "O"],
    currentMark: "X",
}

const setCurrentMark = () => {
    let rtnMark = gameState.currentMark;

    if (rtnMark === gameState.markList[1]) {
        rtnMark = gameState.markList[0];
    }
    else {
        rtnMark = gameState.markList[1];
    }

    gameState.currentMark = rtnMark;
    return rtnMark;
}

const boardDisplay = () => {
    let rowString = ``;

    for (let j = 0; j < boardState.length; j++) {
        currentSpace = j + 1;

        rowString = rowString.concat(` [${boardState[j]}] `);

        if (currentSpace % maxRowLength === 0 && j !== 0) {
            console.log(rowString);
            rowString = ``;
        }
    }
}

const isValidMove = (space) => {
    if (isNaN(space) ||
        typeof (space) !== 'number') {
        console.log("Must be an integer.")
        return false;
    }

    if (!boardState[space]) {
        console.log("Must be in the scope of the board size.")
        return false;
    }

    if (boardState[space] !== "_") {
        console.log("There is already a mark there. Try somewhere else.")
        return false;
    }

    return true;
}

const placeMark = (space) => {
    if (isValidMove(space)) {
        boardState[space] = setCurrentMark();
    }

    else {
        getPlayerMove();
    }
}

const verticalWin = () => {
    let currentCol = [];
    for (let j = 0; j < maxRowLength; j++) {
        let j2 = j + 3;
        let j3 = j2 + 3;
        if (boardState[j] === gameState.currentMark &&
            boardState[j2] === gameState.currentMark &&
            boardState[j3] === gameState.currentMark) {
            return true;
        }
    }
}
const horizontalWin = () => {
    let currentRow;
    for (let i = 0; i < boardState.length; i++) {
        if (i % maxRowLength === 0) {
            currentRow = boardState.slice(i, i + maxRowLength);
        };
        if (currentRow.every(space => space === gameState.currentMark)) {
            return true;
        }
    }
}
const diagonalWin = () => {
    if (boardState[0] === gameState.currentMark &&
        boardState[4] === gameState.currentMark &&
        boardState[8] === gameState.currentMark ||

        boardState[2] === gameState.currentMark &&
        boardState[4] === gameState.currentMark &&
        boardState[6] === gameState.currentMark) {
        return true
    }
    return false;
}

const gameOver = () => {
    if (horizontalWin() ||
        verticalWin() ||
        diagonalWin()) {
        return true;
    }
}

const resetBoard = () => {
    boardState = [];

    for (let space = 0; space < 9; space++) {
        boardState.push('_');
    }
}

let rl = readlineSync;
const getPlayerMove = () => {
    targetRow =
        Number.parseInt(rl.question("Input a row number.\n")) - 1;

    targetCol =
        Number.parseInt(rl.question("Input a coloumn number.\n")) - 1;


    placeMark(findSpace(targetRow, targetCol));
}

for (let i = 0; i < 9; i++) {
    boardDisplay();

    if (gameOver()) {
        console.log(`Player ${gameState.currentMark} wins.`);
        let response = rl.question("Game over. Re-match? (y)\n");
        if (response === "y") {
            i = 0;
            resetBoard();
        }
    }
    else {
        getPlayerMove();
    }
}